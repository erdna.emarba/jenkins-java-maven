package fr.formation.helloworld;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HelloWorldJavaSpringApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	void aDummyTest() {
		assertTrue(2 == 1 + 1, "2 should be equals to 1+1");
	}

}
